Mutt provides several means to
achieve **dynamic** configuration: the various "\*-hook" cmds, the
"`` ` ` ``" (backticks) cmd-substitution known from many shells, and
"macros". You can do already a lot with them as they are, but you can
combine them to achieve more or use them in ways not used before. Add,
correct, or expand the list with your ideas. Keep in mind that with some
variation the tricks can do things not listed here. Get creative.

-----

## (ab)use "macros" as variables.

You can use Mutt macros to store things, some what like the usage of
variables. The key idea is to use macros that dynamically define other
macros or themselves. One use of this is to toggle an option between two
values, so the macro must "remember" the last setting. For example, if
you want the 'S' key to toggle between \`sort=threads\` and
\`sort=date\`, write two muttrc snippets:

``` 
 # ~/.mutt/sort-threads.rc
 set sort=threads
 macro index S "<enter-command>source ~/.mutt/sort-date.rc<enter>"

 # ~/.mutt/sort-date.rc
 set sort=date
 macro index S "<enter-command>source ~/.mutt/sort-threads.rc<enter>"
```

Then add "\`source ~/.mutt/sort-threads.rc\`" (or \`-date\`) to your
main muttrc. You can also use macros instead of the auxillary files (see
[MacroSamples](MacroSamples)), but this is a bit harder due to the quoting necessary.  
Another idea was here:
<http://marc.info/?l=mutt-dev&m=108194999008846&w=2>  
NOTE: since 1.5.13 there is real "generic-vars in mutt" support
("\`my\_...\`"), some things can be done easier with real vars, others
with this trick.

See also: [Keyboard
Macros](https://muttmua.gitlab.io/mutt/manual-dev.html#macro)

## "source" files without error for non-existing files.

Useful for site-installation for many users, some of which might not
have personal aliases (yet).

`` source `FILE=$HOME/.muttaliases; if [ ! -s "$FILE" ]; then
FILE=/dev/null;fi;echo "$FILE"` ``

You might put this `if/then/else` construction into a script for
easier reuse at different places.

`` source `muttsource $HOME/.muttaliases` ``

## monthly set of folders

Maybe in combination with procmail recipes to sort into folders by
month.

`` set folder=$HOME/mail/`date +"%y%m"` ``

or

``` 
 set mbox=$HOME/mail/received-`date +"%y%m"`
 set record=$HOME/mail/sent-`date +"%y%m"`
```

(Beware, `` `date` `` is evaluated only once, so you have to restart
Mutt (or re-source .muttrc) when the month changes)

## Building a list of "mailboxes" on the fly

If you keep "mailboxes" in a separate directory from non-"mailboxes",
then you don't have to manually list them in your muttrc.

`` mailboxes `echo ~/mail/inbox/*` ``

Filename globbing must be enabled in the \`$SHELL\` for this to work.

A more advanced way for maildirs(!) that takes into account subdirs and
other goodies is:

`` mailboxes `find ~/mail/ -type d -name cur -printf '%h '` ``

Using an incantation similar to the above, exclude the .Spam folder:

`` mailboxes `find ~/.maildir/ -type d -name cur -printf '%h\n'|grep -v
'/home/user/.maildir/.Spam$'|tr "\n" " "` ``

And finally, to list both mboxes and maildirs:

`` mailboxes `find ~/mail '(' -type d '(' -name 'cur' -o -name 'new' ')'
-prune -printf '%h ' ')' -o '(' -type f -printf '%p ' ')'` ``

The last line lists any regular file that is not under a new/ or cur/
directory while taking maildirs. Note: if "find" has no -printf, there
is no simple solution to work with "sed", because mbox folders can be
named "new" and "cur", too;

`` mailboxes `$SHELL -c "echo \`find ~/mail -type d '(' -name 'cur' -o
-name 'new' ')' -prune -print | sed -e '/[^/]*$' \`"` ``


`` mailboxes `$SHELL -c "echo \`find ~/mail '(' -type d '(' -name 'cur'
-o -name 'new' -o name 'tmp' ')' -prune ')' -o -type f -print \`"` ``

`-printf` or the '`$SHELL -c "echo...`' work-around is necessary to
avoid new-lines because mutt takes only the 1st line of output from
`` ` ` `` substitutions.

## Expunging old messages

Sometimes you don't want to keep complete archives of mailing list
messages. You can purge old messages almost automatically using the
folder hook command. For
example:

``` 
  `folder-hook ubuntu-users             'push <tag-pattern>~r>1w!~F<enter>'`
```

would tag all messages older than 1 week except flagged ones when
entering a mailbox matching "ubuntu-users". Then apply function
\`<delete-pattern>\` with "\`~T\`" as pattern, and all tagged messages
will be erased as soon as you leave the folder or press the "$" key. Or
for the
brave

``` 
  `folder-hook ubuntu-users             'push <delete-pattern>~r>1w!~F<enter><sync>'`
```

NOTE: The usual hook-traps apply, see [DebugConfig](DebugConfig).

## Spell Checking with Vim

**NOTE:** Vim 7 has spell-checking built-in, use "\`:set spell\`" to
enable.  
To enable it "automatically" when you're
using vim within mutt, put

    ` set editor="vim -c 'set spell spelllang=de,en'" `

in your .muttrc-file. You can use more than one language, e.g. de,en. To
get new language files, please read the vim-help "`:help spelling`"
and "`:help spell-load`".

The following is for Vim 6:  
You can automatically
start a spell checker if you use vim as your editor. Download the
engspchk.tar.bz2 script from
<http://www.vim.org/scripts/script.php?script_id=195> and uncompress it
in your $HOME/.vim dir. Then change your .muttrc file to include the
following:

``` 
  `set editor='vim "+normal \\ec" +/^$/'`
```

This will start up the e-mail editing session in
Spell checking mode, auto-highlighting spelling mistakes for you.

## Sanity check for missing parts/attachments before sending.

Have a look at the [ConfigTricks/CheckAttach](ConfigTricks/CheckAttach) script to be used as
"`$sendmail`" replacement to remind you of missing attachments. Can be
adapted to make any test you need, if you don't trust yourself. Take
care, advanced shell-knowledge recommended.

## Set From header before sending

Add your identities,
example:

``` 
 alias identity_1         Chuck Norris                   <chuck@example.COM>
 alias identity_2         Harry Callahan                 <harry@example.ORG>
 ...
```

Macro: `macro compose v "<edit-from>^Uidentity\_<tab>" "Select from"`  
Write email and while in compose menu press 'v' -
you should see a menu with all your identities, choose one, press Enter,
done.

An extension of this, which lowers the number of keys to press and
supports making other identity-specific settings (e.g. SMTP server to
use), can be found at
<https://web.archive.org/web/20151118125103/http://dione.no-ip.org/wordpress/computing/rotating-identities-with-mutt/>.

See also [User defined
headers](https://muttmua.gitlab.io/mutt/manual-dev.html#my-hdr)

## Generating a dynamic muttrc file

Mutt does not have support for conditional statements in its
configuration file. However, you can create a shell script which outputs
muttrc commands and have Mutt read the output of the script instead.  
Example: Suppose you want to set the value of
"`$editor`" based upon whether or not you are currently using a
graphical user desktop. You might create the following shell script as
"detectgui.sh":
```sh
     #!/bin/sh
     if [ x$DISPLAY != x ]; then
       echo 'set editor="gvim -f"'
     else
       echo 'set editor=vim'
     fi
```
Then in your \`~/.muttrc\`, you can use the following command:

    ` source '~/bin/detectgui.sh|'`

## Generating send-hooks from addresses in a flat text file

I want the following hook to look/work like,

``` 
  send-hook "~t `cat ~/recipients_list` "  'set signature=...
```

I want the matching of everything in the recipients\_list file to be
case insensitive without having to specify each address in the file with
a regex.

Save this script as \`~/bin/hooks.sh\`:
```python
     #!/usr/bin/env python
     for s in open('recipient_list'):
         print 'send-hook ~t "' + s.lower() + '" set signature...'
```
In your ~/.muttrc:

``` 
 source '~/bin/hook.sh|'
```

See also [Syntax of initialization
files](https://muttmua.gitlab.io/mutt/manual-dev.html#muttrc-syntax) and [Reading
initialization commands from another
file](https://muttmua.gitlab.io/mutt/manual-dev.html#source).

## Good Handle For PGP-Inline

You dont like to use procmail to move pgp-inline message into pgp-mime
like here <http://tldp.org/HOWTO/Mutt-GnuPG-PGP-HOWTO-8.html> ?  
You can economize this very large procmail-recipe
into **one** message-hook. This hook also does not destroy german
umlauts
(ÜÖÄ):

``` 
  `message-hook '!~g !~G ~b "^-----BEGIN PGP (SIGNED )?MESSAGE"' 'exec check-traditional-pgp'`
```

See also: [UseGPG](MuttGuide/UseGPG)

## Address Completion in vim

If you configure mutt to edit the headers of your mail while composing
your mails with vim, you can use the autocomplete feature to save some
typing.  
See:
<http://www.vim.org/scripts/script.php?script_id=3128>  
install the mutt-evo-query script in your path
(included in the zip file), copy \`address-search.vim\` to
\`~/.mutt/address-search.vim\`, add to your vimrc:

``` 
  au !BufRead /tmp/mutt* source ~/.mutt/address-search.vim
```

Now while editing your mail type:

    `To: start<C-x><C-u>`

And a you will be presented with a list of all
contact that have "start" as a substring, with <C-u> you cycle through
the matches.
