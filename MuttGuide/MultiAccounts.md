Steal some ideas from
<https://web.archive.org/web/20111028223842/http://www.acoustics.hut.fi/~mara/mutt/profiles.html>
and extend with clues and quirks.

1. hooks, preferably folder-hook for separatly kept mails  
2. scripts for $editor or $sendmail to replace full headers  
3. macros in "compose" to select profile and activate send key, which is unset by default when entering compose mode.
