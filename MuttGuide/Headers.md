### Setting the envelope sender for outgoing messages

Before transmitting the message to the server, the client has to specify
the sender and the recipient of the message with the "FROM" and "RCPT"
SMTP commands. In order to avoid horrible confusion, you need to bear in
mind that the value used during the SMTP session as the sender address
(in the FROM command) may be different from the actual addresses used in
the mail headers (From, Sender, Reply-To headers all with a slightly
different semantic).

The FROM SMTP address is either called envelope sender or envelope from
address or return path and it's finally appended as an actual header
when the message is finally delivered.

The return path value is a sensible value, because this value will be
passed as the next "FROM" SMTP command if the message is relayed by the
server (transmitted to another mail server). This address has to be
meaningful (e.g. it has to be well-formed and its domain has to
correspond to an existing domain) otherwise the message could be
filtered out as spam (as many spam messages don't have a valid return
path). The return path is also, as the name suggests, the address to
which an eventual error message has to be sent, for example in the case
of delivery failure.

It could be set to the address of the postmaster of a site (e.g.
**`postmaster@example.com`**), or it could be set (in the case of a
satellite system) to the address provided by the mail account provider
(e.g.**`john.doe@example.com`**) when the message is relayed to an
external mail server.

By default most SMTP client agents will set the return path to the local
address of the process which spawns them, mutt in our case, so if the
user **alice** in the **`rabbit-house.example.ORG`** system will compose
and send a message with mutt, the SMTP client to which the message is
piped in will use as return path the address
**`alice@rabbit-house.example.ORG`**(it depends on the mailer
configuration).

You can actually force the return path set by the mutt spawned SMTP
client with these
lines:

`set use_envelope_from # a boolean value, you can deselect it with set nouse_envelope_from`  
`set envelope_from_address= "alice@beyond-the-mirror.NET"`

The manual says about use\_envelope\_from:

    When set, mutt will use ``$envelope_from_address''
    as the envelope sender if that is set, otherwise it will attempt to
    derive it from the "From:" header. Note that this information is
    passed to sendmail command using the "-f" command line switch, so
    don't set this option if you are using that switch in $sendmail
    yourself, or if the sendmail on your machine doesn't support that
    command line switch.

Fortunately most SMTP mailers (postfix, exim) mimic the sendmail command
line interface, so this will work with those mailers too.

You may also want to dynamically set the **use\_envelope\_from** and
**envelope\_from\_address** variables using the usual mutt macro/hook
tricks (see [ConfigTricks](ConfigTricks)). A simple way to set it dynamically would be
to set **use\_envelope\_from** and then let mutt use the (eventually
dynamically generated) "From: " header address (set with **my\_hdr** or
with the **from** variable).

Note that in order to be able to perform this operation you may need
special permission: e.g. with exim if your username isn't in the
**untrusted\_set\_sender** list it need either to be in the
**trusted\_users** or in the **trusted\_groups** list to let you set the
return path.

Finally the local server may rewrite the return path (eventually
overriding the mua return path settings), that could result useful when
using a mua not clever as mutt (e.g. mailx, used in many scripts).
