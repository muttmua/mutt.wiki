# Basic Requirements

Please follow the [CodingStyle](CodingStyle) for the project, including proper
indentation.

Give your patch a filename using the convention:
patch-*version*.*initials*.*description*.*revision* where *version* is
the Mutt version the patch is targetted at, *initials* are the initials
(first and last) for your name, *description* is a short (two words at
the max) title for the patch (use underscores if needed), and *revision*
is the version of the patch starting at 1.

An example of the above is *patch-1.5.1.me.long\_qp.1*. Put this into
the file PATCHES in the Mutt source code before running diff, so it gets
included. The output will look similar to this:

```
--- PATCHES~    Tue Nov  6 19:59:33 2001  
+++ PATCHES     Tue Nov  6 19:59:42 2001  
@@ -1,0 +1 @@  
+patch-1.5.1.me.long_qp.1
```

This information gets included in the *mutt -v* output so that when
users complain about bugs, we know who to blame. :-)

# Code Requirements

## Safety Functions

Mutt contains a number of safety wrappers around standard library
functions. Their use can be tested and verified by running the following
script from the top-level source directory:

`./check_sec.sh`

It will run some basic security checks and prints warnings you should
fix prior to submitting the patch; a committer will run these checks
anyway so ensuring your patch is okay may prevent it from being rejected
due to such "violations".

A list of insecure or (under certain conditions) problematic functions
can be obtained with:

`$ grep safe_ lib.h`

Note that a patch may get rejected if their standard library
"equivalents" are used.

## safe\_free() vs. FREE()

Mutt contains a safe\_free() wrapper for the ordinary free() and a macro
around safe\_free() named FREE(). To avoid compiler warnings, the
safe\_free() function though declared as:

`safe_free (void* pointer);`

expects "void\*\*", not "void\*" and dereferences its argument. The
direct usage hence requires some care so that the FREE() macro was
introduced. It is to be used as this:

```
char* foo = ...  
FREE (&foo);
```

so that the check\_sec.sh script can properly test whether FREE() gets
"void\*\*" instead of "void\*".

When hacking on mutt (especially for the first time), please
double-check the use of FREE() and avoid safe\_free().

# Submission

Unified diffs (diff -u) are easier to read for those reviewing the
patches. If you are using Mercurial

`hg diff`

will give you a recursive diff of all files that you have changed.

Send your patch to mutt-dev@mutt.org. Prefix the subject of the message
with **\[PATCH\]** so that it is easily identifiable in a list of
messages.

If your patch doesn't get included in vanilla mutt, you might consider
adding a pointer to your patch on the [PatchList](PatchList) page so others can find
it.
