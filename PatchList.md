This page is for listing third party patches for Mutt core code.
[QueryCommand](QueryCommand) scripts, 3rd party [MuttTools](MuttTools) and [UserPages](UserPages) have their own
places.

  - \[Mutt 1.4/1.5\] <http://www.pizzashack.org/mutt/> -- Derek Martin
    (ddm)

    + pgp-send-menu -- alternative PGP send menu

    + pgp-auto-decode -- automatically decode traditional PGP messages (1.5.8 has this ability)

  - \[Mutt 1.2/1.3\] <http://home.worldonline.dk/byrial/mutt/patches/>
    -- Byrial Ole Jensen
(bj)

    + noquote_hdr_term -- don't quote the header terminator when replying with headers

    + status-time -- include current time in status line

    + my_hdr_subject -- execute send-hooks before setting subject

    + hash_destroy -- speed up a little bit closing of mailboxes

  - \[Mutt 1.4/1.5\] <http://cedricduval.free.fr/mutt/patches/> --
    Cedric Duval
(cd)

    + trash_folder and purge_message -- move deleted messages to trash folder

    + my_list -- lists for patterns, hooks, mailboxes and subscribe/lists

    + signatures_menu -- menu to choose signatures

    + ifdef -- check for compile time features

    + pattern_broken -- match broken threads

    + source_multiple -- source multiple files at once

  - \[Mutt 1.5\] <http://www.schrab.com/aaron/mutt/> -- Aaron Schrab
    (ats)

    + date_conditional -- vary format of dates in the index based on how recent the message is

    + mark_old -- make mark_old a quad option

    + parent_match -- pattern matching against parent message

  - \[Mutt 1.3/1.5\] <http://www.rachinsky.de/nicolas/mutt.shtml> --
    Nicolas Rachinsky
(nr)

    + crypt-autohook -- autosign/autoencrypt when recipient keys are available

  - \[Mutt 1.3-1.4\] <http://www.42.org/~sec/mutt/> -- Stefan Zehl
(sec)

    + _A -- %A displays number of messages in pager_format

    + backlog -- add X-Current-Backlog: header to outgoing messages

    + keypad -- work around broken ncurses on FreeBSD 2.2.7

    + pager_status_on_top -- colored pager status on top/bottom

    + reverse_reply -- use realname from aliases in replies [see below for newer version]

    + mailcap-fix -- fix mailcap handling for mails containing only a non displayable attachment

    + pgp_shorten -- shorten PGP output length

    + previous_jump -- jump back to message last jumped from

  - \[Mutt 0.91-1.5\] <http://www.spinnaker.de/mutt/compressed/> --
    Roland Rosenfeld (rr)

    + compressed -- support for compressed/encrypted folders

  - \[Mutt 1.1-1.5\] <http://mutt.org.ua/download/> -- Vsevolod Volkov
    (vvv)

    + compressed -- support for compressed/encrypted folders *[use
the patch from spinnaker.de instead! --
Myon]*

    + initials -- author initials in index_format

    + nntp -- NNTP/news folder support

    + quote -- finetune format of quoted text

    + slang -- alternate thread tree for linking with slang

  - \[Mutt 1.4/1.5\] <http://www.woolridge.org/mutt/> -- Dale Woolridge
    (dw)

    + maildir-mtime -- sort maildirs by latest entry in new/ in folder brower

    + pgp-menu-traditional -- traditional PGP signatures in compose menu

    + confirm-crypt-hook -- allows you to skip the "Use keyID" prompt

    + multiple-crypt-hook -- multiple crypt-hooks with the same pattern, multiple keyIDs per pattern

    + crypt-autoselectkey -- autoselect key when only one available in "PGP keys" menu

    + mbox-hook -- printf-like sequences in mbox-hook mailbox argument

    + pgp-timeout -- increase timeout period for pgp_timeout

    + pgp-hook -- combines confirm-crypt-hook, multiple-crypt-hook, and crypt-autoselectkey

    + urlview-html -- make urlview produce HTML

  - \[Mutt 1.4/1.5\] <http://home.uchicago.edu/~dgc/mutt/> -- David
    Champion
(dgc)

    + krb -- fix MIT Kerberos 1.2.4 linking problems

    + xlabel-ext -- edit X-Label: headers (an alternative way without patching is in [UserPages](UserPages)). [See also the xlabel branch on <https://github.com/gi1242/mutt> for a current version.]

    + unbind -- unbind key bindings

    + attach -- count and display attachments in index

    + deepif -- nested if-else sequences in format strings

    + isalias -- pattern matching against aliases

    + markmsg -- set jump marks on messages

    + softfill -- pad space in index_format: <http://home.uchicago.edu/~dgc/sw/mutt/patch-1.5.6.dgc.softfill.2>

    + fmtpipe -- use pipes in format strings

  - \[Mutt 1.5\] <http://debian.lpr.ch/Mutt/> -- Lukas P. Ruf
(lpr)

    + signin and signoff -- greeting and regrad pre/appended to outgoing messages

    + collapse_flagged -- do not collapse threads containing flagged messages

  - \[Mutt ??\]
    <http://tobbe.nu/blog/2008/04/04/mutt-inform-screen-patch/> --
    Tobias Bengtsson (ydo)

    + inform_screen -- send escape code to screen on new mail

  - \[Mutt 1.5\] <http://www.wolfermann.org/mutt.html> -- Armin
    Wolfermann (aw)

    + jumptagged -- jump forward/backward between tagged messages

    + listreply -- ask when sending list message replies to author only

    + timeouthook -- call mutt commands when $timeout expires

  - \[Mutt 1.5\] <http://lunar-linux.org/index.php?page=mutt-sidebar> --
    Terry P. Chan

    + sidebar -- nice sidebar that shows folder list

    + past versions: [Justin
Hibbits](http://vorlon.cwru.edu/~jrh29/mutt/index.html), [Thomer
M. Gil](http://thomer.com/mutt/)

  - \[Mutt ??\]
<http://savannah.nongnu.org/projects/mutt-guile>

    + mutt-guile -- Extensible Mutt: scripting language for Mutt using Guile

    + mutt-guile Wiki: <http://amacleod.is-a-geek.org/mutt-guile/> (set up by AllisterMacLeod)

  - \[Mutt ??\] <http://www.emaillab.org/mutt/download15.html.en>,
    [SourceForge
    page](http://sourceforge.jp/frs/index.php?group_id=351),
    [download](http://www.emaillab.org/mutt/download159.html) --
    Takizawa Takashi \[Mutt
1.2-1.5\]

    + ja -- japanese patch

    + ja manual: <http://www.emaillab.org/mutt/1.5/doc/manual-ja-patch.en.txt>

    + assumed_charset -- charsets which are to be used for header fields and message body with no encoding indication

    + file_charset (attach_charset) -- list of charsets used for autosensing the charset of a text file when you attach it.

    + create_rfc2047_parameters -- add rfc2047-encoded parameters to Content-Type. this is prohibited by the standard, but some buggy mail clients require this

    + nonascii_old_pgp -- [1.4] (pgp_charsethack is available for greater than 1.5.8)

  - \[Mutt 1.5\]
    <http://wwwcip.informatik.uni-erlangen.de/~sithglan/mutt/> -- Thomas
    Glanzmann (tg)

    + color-status -- colorize status line

  - \[Mutt 1.4\] <http://dwyn.net/mutt/> -- Tudor Bosman

    + headercache -- for IMAP (old version, see above for new)

  - \[Mutt 1.5\] <http://www.df7cb.de/projects/mutt/> -- Christoph Berg
    (cb,
Myon)

    + chdir -- 'cd' command to change mutt's current directory

    + reverse_reply -- use realname from aliases in replies (similar to $reverse_alias)

    + xface -- updated slrnface patch to display X-Face headers in mutt

  - \[Mutt 1.5\] <ftp://ftp.gnupg.org/gcrypt/alpha/aegypten> -- Werner
    Koch
(wk)

    + cryptmod, gpgme -- gpgme support for Mutt (1.5.8 has this ability) see also <http://www.gnupg.org/aegypten2/> and <87u0zkib7m.fsf@vigenere.g10code.de>

  - \[Mutt 1.4/1.5\] <http://www.deez.info/sengelha/code/mutt-libesmtp/>
    -- Steven Engelhard
(sde)

    + libesmtp -- use libESMTP instead of an external MTA to send mail

    + void_passphrase_on_failed_sign -- automatically forget wrong passphrase

  - \[Mutt 1.4\] <ftp://ftp.sonic.net/pub/users/enrico/mutt> -- G. Paul
    Ziemba (gpz)

    + tagged-macros -- make tag-prefix play nice with macros

  - \[Mutt 0.xx\] <http://www.fiction.net/blong/programs/mutt/> --
    Brandon Long
(bl)

    + cygwin -- Cygwin32 NT/95 Port (old)

    + mx_nntp -- NNTP patch (old)

    + content_md5 -- content-MD5 (RFC 1864) support (old)

    + folder_count -- count number of messages in folders in folder list (old)

    + uudeview -- uuencoding, xxencoding, and BinHEX support (very old)

    + altpgp -- alternative PGP view (very old)

  - \[Mutt 1.5\] <http://www.elho.net/dev/mutt/> -- Elmar Hoffmann (eh)

    + thread_subject -- make hiding of subjects in a thread optional

  - \[Mutt 1.5\] <http://www.plumlocosoft.com/software/> -- Eric
    Hustvedt
(reh)

    + imap_fcc_status -- prevent saving of Status: headers when using fcc with IMAP. Intended for use with Courier's IMAP outbox support.

  - \[Mutt 1.5\]
    <http://www.spocom.com/users/gjohnson/mutt/patches.html> -- Gary
    Johnson
(gj)

    + stuff_all_quoted -- improve the appearance of format=flowed messages

    + attach_sanitize -- sanitize file names when saving attachments

    + sigontop_space_fix -- add blank line following 'sigontop' signature

  - \[Mutt 1.5\] <http://greek0.net/mutt.html> -- Christian Aichinger
    (Greek0)

    + indexcolor -- custom colors for author, subject, date, etc. in the index display

  - \[Mutt 1.5\] <http://www.momonga-linux.org/~tamo/> --
Tamo

    + patch-1.5.8.tamo.patterns.3 -- pattern-menu on pattern-prompt (tab key after ~ will show it)

    + patch-1.5.9.tamo.POTFILE.4 -- improve GPGME code

    + patch-1.5.8.tamo.comval_help.2 -- status-commands

    + <http://does-not-exist.org/mail-archives/mutt-dev/msg01444.html> -- smooth scroll

    + <http://tamo.tdiary.net/20081107.html#p01> -- ":set" auto-complete for path

  - \[Mutt ??\]
    <http://www.ogersoft.at/freepatches/mutt/ignore-thread.patch> --
    Gerhard
Oettl

    + ignore-thread -- permanently ignore threads

    + updated for 1.5.20 at <http://ben.at.tanjero.com/patches/ignore-thread-1.5.20.patch>

  - \[Mutt ??\] <http://expass.de/mutt> -- Frank
Luithle

    + mutt-1.5.9_pgp-unverbose-patch.diff -- same as pgp_shorten by sec@42.org, but for version 1.5.9

  - \[Mutt ??\]
    [patch-1.5.11.as.echo.1](http://permalink.gmane.org/gmane.mail.mutt.devel/10320)
    - new echo command for visual feedback -- AdamSpiers

<!-- end list -->

  - \[Mutt ??\] PluginSupport -- add support for dynamically loading
    extensions to Mutt

<!-- end list -->

  - \[Mutt 1.5\] <http://bjk.sourceforge.net/pwmd/> -- Ben Kibbey
(bjk)

    + Password Manager Daemon (pwmd). Fetches auth/server info from pwmd.

## Archived patches (included in mutt)

  - \[Mutt 1.4/1.5\] <http://cedricduval.free.fr/mutt/patches/> --
    Cedric Duval (cd)

    + edit_threads -- link and break threads  (included in 1.5.10)

  - \[Mutt 1.5\]
    <http://wwwcip.informatik.uni-erlangen.de/~sithglan/mutt/> -- Thomas
    Glanzmann
(tg)

    + header-cache -- for maildir / imap (included in 1.5.9)

    + mutt-thread -- match whole threads containing messages matching patterns (extends threadcomplete; included in 1.5.12)

  - \[Mutt 1.5\] <http://www.df7cb.de/projects/mutt/> -- Christoph Berg
    (cb,
Myon)

    + current_shortcut -- makes the "^" character a shortcut for the currently open mailbox (new version) (1.5.10 has this ability)

    + menu_context -- context lines for menus (similar to $pager_context) (1.5.8 has this ability)

    + thread_pattern -- obsolete, see Thomas Glanzmann's *mutt-thread* above

  - \[Mutt 1.5\] <http://www.vinc17.org/mutt/index.en.html> -- Vincent
    Lefèvre

    + patch-1.5.6.vl.savehist.1 -- persistant prompt history of commands, files, patterns... (applied in 1.5.15)

  - \[Mutt ??\] <http://mutt.kublai.com/> -- Brendan
Cully

    + patch-1.5.11.bc.smtp.10 -- built-in ESMTP relay support. (applied in 1.5.15)
