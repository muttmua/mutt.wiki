## !UserStory/GMailOverIMAP

This is a quick guide to setting up mutt to work with GMail's IMAP
interface.

### Essentials: configuration options

``` 
  set imap_user = 'yourusername@gmail.com'
  set imap_pass = 'yourpassword'

  set folder = imaps://imap.gmail.com/
  set spoolfile = +INBOX
  set record = "+[Gmail]/Sent Mail"
  set postponed = "+[Gmail]/Drafts"
```

If you want to use GMail SMTP server, set record to an empty string as
it handles saving the sent mail anyway.

``` 
  set smtp_url = 'smtps://yourusername@smtp.gmail.com'
  set smtp_pass = 'yourpassword'
  set record=""
```

You can also set the mbox:

``` 
  set mbox="imaps://imap.gmail.com/[Gmail]/All Mail"
```

...which means that when you exit mutt, it will prompt you to archive
your read messages.

If you are using the [trash\_folder
patch](http://cedricduval.free.fr/mutt/patches/#trash)

``` 
  set trash="imaps://imap.gmail.com/[Gmail]/Trash"
```

### Navigation quirks

You can change folders using the change-folder command, but you'll need
to hit **\<space\>** to view the files instead of just changing folders.

### Web keyboard shortcut macros

``` 
  bind editor <space> noop
  macro index,pager y "<save-message>=[Gmail]/All Mail<enter><enter>" "Archive"
  macro index,pager d "<save-message>=[Gmail]/Trash<enter><enter>" "Trash"
  macro index gi "<change-folder>=INBOX<enter>" "Go to inbox"
  macro index ga "<change-folder>=[Gmail]/All Mail<enter>" "Go to all mail"
  macro index gs "<change-folder>=[Gmail]/Starred<enter>" "Go to starred messages"
  macro index gd "<change-folder>=[Gmail]/Drafts<enter>" "Go to drafts"
```

You need the "noop" `bind` so that the line editor accepts IMAP
folders with spaces in their names. The gi, ga, gs and gd shortcuts help
get around the "navigation quirks" mentioned above too.
